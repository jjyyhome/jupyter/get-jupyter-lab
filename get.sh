#!/usr/bin/env bash

git submodule init

git submodule add https://github.com/kkoojjyy/jupyterlab.git jupyter/jupyterlab

git submodule add https://github.com/topics/jupyterlab-extension jupyter/jupyterlab-ext

git submodule add https://github.com/kkoojjyy/jupyterlab-hub.git jupyter/jupyterlab-hub

git submodule add https://github.com/kkoojjyy/jupyterlab_voyager.git jupyter/jupyterlab_voyager

git submodule add https://github.com/kkoojjyy/jupyterlab_app.git jupyter/jupyterlab_app

git submodule add https://github.com/kkoojjyy/jupytext.git  jupyter/jupytext

#目录扩展
git submodule add https://github.com/kkoojjyy/jupyterlab-toc.git jupyter/jupyterlab-toc

#快节键

git submodule add https://github.com/kkoojjyy/jupyterlab-vim.git jupyter/jupyter-vim

git submodule add https://github.com/kkoojjyy/jupyterlab-git.git jupyter/jupyterlab-git

git submodule add https://github.com/timkpaine/jupyterlab_templates.git jupyter/jupyterlab_templates

git submodule add https://github.com/kkoojjyy/jupyterlab-sidecar.git jupyter/jupyterlab_sidecar

git submodule add  https://github.com/kkoojjyy/jupyterlab-github.git jupyter/jupyterlab-github


git submodule sync
git submodule update